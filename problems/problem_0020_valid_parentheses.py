"""
20. Valid Parentheses
Easy
https://leetcode.com/problems/valid-parentheses/

Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

An input string is valid if:
    Open brackets must be closed by the same type of brackets.
    Open brackets must be closed in the correct order.
    Every close bracket has a corresponding open bracket of the same type.

Example 1:
Input: s = "()"
Output: true

Example 2:
Input: s = "()[]{}"
Output: true

Example 3:
Input: s = "(]"
Output: false

Constraints:
    1 <= s.length <= 104
    s consists of parentheses only '()[]{}'.
"""

from queue import LifoQueue

symbols = {
    ")": "(",
    "}": "{",
    "]": "[",
}

class Solution:
    def isValid(self, s: str) -> bool:
        q = LifoQueue(maxsize=len(s))

        for char in s:
            # If this is open bracket
            if char in symbols.values():
                q.put(char)
            # If this is close bracket
            else:
                if q.empty():
                    return False
                top = q.get()
                if symbols[char] != top:
                    return False
        return q.empty()
