"""
67. Add Binary
Easy
https://leetcode.com/problems/add-binary/

Given two binary strings a and b, return their sum as a binary string.

Example 1:
Input: a = "11", b = "1"
Output: "100"

Example 2:
Input: a = "1010", b = "1011"
Output: "10101"

Constraints:
    1 <= a.length, b.length <= 104
    a and b consist only of '0' or '1' characters.
    Each string does not contain leading zeros except for the zero itself.
"""


class Solution:
    def addBinary(self, a: str, b: str) -> str:
        max_len = max(len(a), len(b))
        result_list = [0] * (max_len + 1)

        to_add = 0
        for i in range(max_len):
            left_part = int(a[-i - 1]) if len(a) > i else 0
            right_part = int(b[- i - 1]) if len(b) > i else 0

            num_sum = left_part + right_part + int(to_add)
            result_list[- i - 1] = str(num_sum % 2)
            to_add = num_sum // 2

        if to_add:
            result_list[0] = "1"
            return "".join(result_list)
        return "".join(result_list[1:])
