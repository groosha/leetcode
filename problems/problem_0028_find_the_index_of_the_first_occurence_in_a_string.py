"""
28. Find the Index of the First Occurrence in a String
Easy
https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/

Given two strings needle and haystack, return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

Example 1:
Input: haystack = "sadbutsad", needle = "sad"
Output: 0
Explanation: "sad" occurs at index 0 and 6.
The first occurrence is at index 0, so we return 0.

Example 2:
Input: haystack = "leetcode", needle = "leeto"
Output: -1
Explanation: "leeto" did not occur in "leetcode", so we return -1.

Constraints:
    1 <= haystack.length, needle.length <= 104
    haystack and needle consist of only lowercase English characters.
"""
class Solution:
    def strStr(self, haystack: str, needle: str) -> int:

        l_needle = len(needle)
        matches: dict[int, int] = dict()

        for i in range(len(haystack)):
            for key, value in matches.items():
                if value == -1:
                    continue
                if haystack[i] == needle[value]:
                    if l_needle == value + 1:
                        return key
                    matches[key] = value + 1
                else:
                    matches[key] = -1
            if haystack[i] == needle[0]:
                if l_needle == 1:
                    return i
                matches[i] = 1
        return -1
