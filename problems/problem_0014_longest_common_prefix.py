"""
14. Longest Common Prefix
Easy

Write a function to find the longest common prefix string amongst an array of strings.
If there is no common prefix, return an empty string "".

Example 1:
Input: strs = ["flower","flow","flight"]
Output: "fl"

Example 2:
Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.


Constraints:
    1 <= strs.length <= 200
    0 <= strs[i].length <= 200
    strs[i] consists of only lower-case English letters.

"""
from typing import List


class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        # Special case
        if len(strs) == 1:
            return strs[0]

        # If we sort our array, the shortest words will be checked first
        strs = sorted(strs)
        # ... or we can find an empty string and return fast
        if strs[0] == "":
            return ""

        # Compare first 2 words
        longest_prefix_count = 0
        for i in range(min(len(strs[0]), len(strs[1]))):
            if strs[0][i] == strs[1][i]:
                longest_prefix_count += 1
            else:
                break
        # If these words don't have anything in common, return empty string
        if longest_prefix_count == 0:
            return ""
        # If there are only 2 words, return their common prefix
        if len(strs) == 2:
            return strs[0][:longest_prefix_count]
        # Check the first word with all other words
        for i in range(2, len(strs)):
            # If first and Nth word have less symbols in common, update longest prefix
            for symbol_index in range(longest_prefix_count):
                if strs[0][symbol_index] != strs[i][symbol_index]:
                    if symbol_index < longest_prefix_count:
                        longest_prefix_count = symbol_index
                        break
            if longest_prefix_count == 0:
                return ""

        return strs[0][:longest_prefix_count]
