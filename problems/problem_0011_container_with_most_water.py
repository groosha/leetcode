"""
11. Container With Most Water
Medium

Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai).
n vertical lines are drawn such that the two endpoints of the line i is at (i, ai) and (i, 0).
Find two lines, which, together with the x-axis forms a container, such that the container contains the most water.
Notice that you may not slant the container.


Example 1:
Input: height = [1,8,6,2,5,4,8,3,7]
Output: 49
Explanation: The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue section) the container can contain is 49.

Example 2:
Input: height = [1,1]
Output: 1

Example 3:
Input: height = [4,3,2,1,4]
Output: 16

Example 4:
Input: height = [1,2,1]
Output: 2


Constraints:
    n == height.length
    2 <= n <= 105
    0 <= height[i] <= 104
"""

from typing import List


class Solution:
    def maxArea(self, height: List[int]) -> int:
        total_max = 0
        left_index = 0
        right_index = len(height)-1

        while left_index < right_index:
            left_value = height[left_index]
            right_value = height[right_index]
            water = (right_index - left_index) * min(left_value, right_value)
            if water > total_max:
                total_max = water

            if right_value < left_value:
                right_index -= 1
            else:
                left_index += 1
        return total_max
