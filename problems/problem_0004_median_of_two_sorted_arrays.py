"""
4. Median of Two Sorted Arrays
Hard

Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
The overall run time complexity should be O(log (m+n)).

Example 1:
Input: nums1 = [1,3], nums2 = [2]
Output: 2.00000
Explanation: merged array = [1,2,3] and median is 2.

Example 2:
Input: nums1 = [1,2], nums2 = [3,4]
Output: 2.50000
Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.

Constraints:
    nums1.length == m
    nums2.length == n
    0 <= m <= 1000
    0 <= n <= 1000
    1 <= m + n <= 2000
    -106 <= nums1[i], nums2[i] <= 106
"""
from time import monotonic
from typing import List


class Solution:
    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        """
        This is a "manual" solution, which doesn't use any built-ins.
        However it is written in plain Python, so it is 4x-5x SLOWER than
        findMedianSortedArraysOld() method
        """
        result = []

        """
        We will "merge" two lists virtually, which means no new list is created.
        We just need particular indexes of such "virtual list", which are needed to count median
        If list size is even, then we need two values. One, if list size is odd.
        Also we count "upper_boundary" - an index after which we can 100% stop, since all necessary indexes are found
        """
        if (len_merged := len(nums1) + len(nums2)) % 2 == 0:
            indexes = [int(len_merged / 2), int(len_merged / 2 - 1)]
        else:
            indexes = [int((len_merged - 1) / 2)]
        upper_boundary = indexes[-1] + 1

        i = j = 0
        count = -1

        # A small optimization, reduces len() count gradually, though its effect is very small.
        len_nums1 = len(nums1)
        len_nums2 = len(nums2)
        while count < upper_boundary:
            count += 1
            # If reached the end of the first list
            if i == len_nums1:
                # If we need the current index to count the median
                if count in indexes:
                    result.append(nums2[j])
                j += 1
            else:
                # If reached the end of the second list
                if j == len_nums2:
                    if count in indexes:
                        result.append(nums1[i])
                    i += 1
                # If didn't reach the end of any list
                else:
                    if nums1[i] < nums2[j]:
                        next_val = nums1[i]
                        i += 1
                    else:
                        next_val = nums2[j]
                        j += 1
                    if count in indexes:
                        result.append(next_val)
        if len(result) == 1:
            return result[0]
        else:
            return (result[0] + result[1]) / 2

    def findMedianSortedArraysOld(self, nums1: List[int], nums2: List[int]) -> float:
        """
        My first solution of this problem, relies on sorted() built-in function.
        Apparently this is much faster, because sorted() is written in C (if using "classic" CPython, of course).
        """
        new_list = sorted(nums1 + nums2)
        l_n = len(new_list)
        if l_n % 2 == 0:
            return (new_list[int(l_n / 2) - 1] + new_list[int(l_n / 2)]) / 2
        else:
            return new_list[int((l_n - 1) / 2)]

    def test_problem(self):
        """
        A sample test function if you want to compare run time yourself
        """
        array1 = [1, 4, 10, 15]
        array2 = [1, 2, 5, 11, 12]

        runs = 100_000

        start = monotonic()
        for i in range(runs):
            self.findMedianSortedArrays(array1, array2)
        finish = monotonic()
        print(f"[new] Finished in {finish - start} seconds")

        start = monotonic()
        for i in range(runs):
            self.findMedianSortedArraysOld(array1, array2)
        finish = monotonic()
        print(f"[old] Finished in {finish - start} seconds")


if __name__ == '__main__':
    s = Solution()
    s.test_problem()
