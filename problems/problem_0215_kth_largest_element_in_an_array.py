"""
215. Kth Largest Element in an Array
Medium
https://leetcode.com/problems/kth-largest-element-in-an-array/

Given an integer array nums and an integer k, return the kth largest element in the array.
Note that it is the kth largest element in the sorted order, not the kth distinct element.
Can you solve it without sorting?

Example 1:
Input: nums = [3,2,1,5,6,4], k = 2
Output: 5

Example 2:
Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
Output: 4

Constraints:
    1 <= k <= nums.length <= 105
    -104 <= nums[i] <= 104
"""
from typing import List
from queue import PriorityQueue


class Solution:
    def findKthLargest(self, nums: List[int], k: int) -> int:
        q = PriorityQueue()

        if len(nums) == 1:
            return int(k == nums[0])

        for num in nums:
            q.put(-num)
            # print(f"Put number {num} to queue.")
        res = None
        # print(f"Queue final state: {list(q.queue)}")
        for _ in range(k):
            res = q.get()
            # print(f"Removed {res}")
        return -res
