"""
Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).



Example 1:

Input: x = 123
Output: 321

Example 2:

Input: x = -123
Output: -321

Example 3:

Input: x = 120
Output: 21

Example 4:

Input: x = 0
Output: 0
"""


class Solution:
    def reverse(self, x: int) -> int:
        int_min = -2**31
        int_max = 2**31-1
        is_negative = x < 0

        nums = [symbol for symbol in str(abs(x))]
        new_number = int("".join(reversed(nums)))
        if new_number > int_max or new_number < int_min:
            return 0
        if is_negative:
            return -new_number
        else:
            return new_number
