"""
6. ZigZag Conversion

The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R

And then read line by line: "PAHNAPLSIIGYIR"
Write the code that will take a string and make this conversion given a number of rows:
string convert(string s, int numRows);

Example 1:
Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"

Example 2:
Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
P     I    N
A   L S  I G
Y A   H R
P     I

Example 3:
Input: s = "A", numRows = 1
Output: "A"

Constraints:
    1 <= s.length <= 1000
    s consists of English letters (lower-case and upper-case), ',' and '.'.
    1 <= numRows <= 1000
"""


class Solution:
    def convert(self, s: str, numRows: int) -> str:
        # Corner cases
        if numRows == 1 or len(s) <= numRows:
            return s

        # Initializa empty arrays
        arr = [[] for i in range(numRows)]

        # Every "batch" consists of 1 vertical line
        # and diagonally placed symbols up to the next vertical line
        batch_size = numRows + (numRows - 2)

        for symbol_index, symbol in enumerate(s):
            local_index = symbol_index % batch_size

            # Going down
            if local_index < numRows:
                arr[local_index].append(symbol)
            # Going up & sideways
            else:
                arr[batch_size - local_index].append(symbol)

        result = []
        for array in arr:
            result.append("".join(array))
        return "".join(result)
