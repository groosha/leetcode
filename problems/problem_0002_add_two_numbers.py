"""
2. Add two numbers

You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order,
and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example 1:
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.

Example 2:
Input: l1 = [0], l2 = [0]
Output: [0]

Example 3:
Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]


Constraints:
    The number of nodes in each linked list is in the range [1, 100].
    0 <= Node.val <= 9
    It is guaranteed that the list represents a number that does not have leading zeros.
"""


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

    # For convenience
    # def __eq__(self, other):
    #     self_array = [self.val]
    #     if self.next is not None:
    #         me = self
    #
    #         while me.next is not None:
    #             me = me.next
    #             self_array.append(me.val)
    #
    #     other_array = [other.val]
    #     while other.next is not None:
    #         other = other.next
    #         other_array.append(other.val)
    #
    #     return self_array == other_array

    @classmethod
    def from_int(cls, x: int):
        array = [int(symbol) for symbol in str(x)]
        array.reverse()

        out = ListNode(val=array[-1])
        if len(array) > 1:
            for num in reversed(array[:-1]):
                out = ListNode(num, out)
        return out


class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:
        l1_arr = []
        l2_arr = []

        l1_arr.append(l1.val)
        while l1.next is not None:
            l1 = l1.next
            l1_arr.append(l1.val)
        l1_arr.reverse()

        l2_arr.append(l2.val)
        while l2.next is not None:
            l2 = l2.next
            l2_arr.append(l2.val)
        l2_arr.reverse()

        l1_value = int("".join(str(x) for x in l1_arr))
        l2_value = int("".join(str(x) for x in l2_arr))

        result_num = l1_value + l2_value
        return ListNode.from_int(result_num)
