"""
42. Trapping Rain Water
Hard

Given n non-negative integers representing an elevation map where the width of each bar is 1,
compute how much water it can trap after raining.

Example 1:
Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
Output: 6
Explanation: The above elevation map (black section) is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped.

Example 2:
Input: height = [4,2,0,3,2,5]
Output: 9

Constraints:
    n == height.length
    1 <= n <= 2 * 104
    0 <= height[i] <= 105
"""
from typing import List


class Solution:
    # Left-right solution
    def trap(self, height: List[int]) -> int:
        l_to_r = [0] * len(height)
        r_to_l = [0] * len(height)
        result = 0

        l_to_r[0] = height[0]
        for i in range(1, len(height)):
            l_to_r[i] = max(height[i], l_to_r[i - 1])

        r_to_l[-1] = height[-1]
        for i in range(len(height) - 2, -1, -1):
            r_to_l[i] = max(height[i], r_to_l[i + 1])

        for i in range(len(height)):
            result += (min(l_to_r[i], r_to_l[i]) - height[i])

        return result
