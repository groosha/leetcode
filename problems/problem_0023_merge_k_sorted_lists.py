"""
23. Merge k Sorted Lists
Hard

You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.
Merge all the linked-lists into one sorted linked-list and return it.

Example 1:
Input: lists = [[1,4,5],[1,3,4],[2,6]]
Output: [1,1,2,3,4,4,5,6]
Explanation: The linked-lists are:
[
  1->4->5,
  1->3->4,
  2->6
]
merging them into one sorted list:
1->1->2->3->4->4->5->6

Example 2:
Input: lists = []
Output: []

Example 3:
Input: lists = [[]]
Output: []

Constraints:
    k == lists.length
    0 <= k <= 104
    0 <= lists[i].length <= 500
    -104 <= lists[i][j] <= 104
    lists[i] is sorted in ascending order.
    The sum of lists[i].length will not exceed 104.
"""
from typing import List, Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeKLists(self, lists: List[Optional[ListNode]]) -> Optional[ListNode]:
        head = ListNode(val=0)
        current = head
        last_min = 105
        while True:
            min_value = 105
            index_to_move = -1

            linked_list: Optional[ListNode]
            for index, linked_list in enumerate(lists):
                # Skip "exhausted" linked lists
                if linked_list is None:
                    continue
                if linked_list.val < min_value:
                    index_to_move = index
                    min_value = linked_list.val

                    # A small optimization to break
                    # if this is the smallest value in current loop
                    if min_value == last_min:
                        break

            if index_to_move == -1:
                break

            current.next = ListNode(val=lists[index_to_move].val)
            current = current.next
            last_min = lists[index_to_move].val

            # Move selected linked list forward
            lists[index_to_move] = lists[index_to_move].next

        return head.next
