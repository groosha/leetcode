from typing import List

import pytest

from explore.recursion.stringreverse import Solution

s = Solution()


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [["h", "e", "l", "l", "o"], ["o", "l", "l", "e", "h"]],
        [["H", "a", "n", "n", "a", "h"], ["h", "a", "n", "n", "a", "H"]],
    ],
)
def test_problem(x: List[str], expected_result: List[str]):
    s.reverseString(x)
    assert x == expected_result
