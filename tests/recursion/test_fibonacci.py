from typing import List, Optional

import pytest

from explore.recursion.fibonacci import Solution

s = Solution()


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [2, 1],
        [3, 2],
        [4, 3],
    ],
)
def test_problem(x: int, expected_result: int):
    assert s.fib(x) == expected_result
