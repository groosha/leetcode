from typing import List, Optional

import pytest

from explore.recursion.reverse_linked_list import Solution, ListNode

s = Solution()


def make_linked_list(array: List[int]) -> Optional[ListNode]:
    if len(array) == 0:
        return None
    elif len(array) == 1:
        return ListNode(val=array[0])

    head = ListNode(val=array[0])
    current = head
    for i in range(1, len(array)):
        next_node = ListNode(val=array[i])
        current.next = next_node
        current = next_node
    return head


def extract_values(head: Optional[ListNode]) -> List[int]:
    if head is None:
        return []
    elif head.next is None:
        return [head.val]

    result = []
    while head is not None:
        result.append(head.val)
        head = head.next
    return result


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [[1, 2, 3, 4, 5], [5, 4, 3, 2, 1]],
        [[1, 2], [2, 1]],
        [[], []],
    ],
)
def test_problem(x: List[int], expected_result: List[str]):
    ll_to_test = make_linked_list(x)
    result = s.reverseList(ll_to_test)
    assert extract_values(result) == expected_result
