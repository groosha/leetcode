import pytest

from explore.recursion.climbing_stairs import Solution

s = Solution()


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [2, 2],
        [3, 3],
    ],
)
def test_problem(x: int, expected_result: int):
    assert s.climbStairs(x) == expected_result
