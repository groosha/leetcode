from typing import List

import pytest

from explore.recursion.pascal_triangle import Solution

s = Solution()


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [3, [1, 3, 3, 1]],
        [0, [1]],
        [1, [1, 1]],
    ],
)
def test_problem(x: int, expected_result: List[int]):
    assert s.getRow(x) == expected_result
