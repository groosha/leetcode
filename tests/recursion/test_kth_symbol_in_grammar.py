import pytest

from explore.recursion.kth_symbol_in_grammar import Solution

s = Solution()


@pytest.mark.parametrize(
    "n, k, expected_result",
    [
        [1, 1, 0],
        [2, 1, 0],
        [2, 2, 1],
        [3, 1, 0],
        [4, 1, 0],
        [5, 1, 0],
        [6, 1, 0],
        [7, 1, 0],
        [8, 1, 0],
        [9, 1, 0],
        [4, 5, 1]
    ],
)
def test_problem(n: int, k: int, expected_result: int):
    assert s.kthGrammar(n, k) == expected_result
