from typing import List, Tuple, Optional

import pytest

from explore.recursion.binary_search import Solution, TreeNode
from tests.recursion.test_maximum_depth_of_binary_tree import build_tree_from_array

s = Solution()


def build_array_from_tree(root: Optional[TreeNode]) -> List[Optional[int]]:
    if root is None:
        return []

    result = []
    if root.left is not None:
        result.append(root.left.val)
    if root.right is not None:
        result.append(root.right.val)

    if root.left is not None:
        result += build_array_from_tree(root.left)
    if root.right is not None:
        result += build_array_from_tree(root.right)
    return result


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [([4, 2, 7, 1, 3], 2), [2, 1, 3]],
        [([4, 2, 7, 1, 3], 5), []],
    ],
)
def test_problem(x: Tuple[List[int], int], expected_result: List[str]):
    original_tree = build_tree_from_array(x[0])
    result_tree = s.make_search(original_tree, x[1])
    result_array = build_array_from_tree(result_tree)

    if result_tree is not None:
        result_array.insert(0, result_tree.val)

    assert result_array == expected_result
