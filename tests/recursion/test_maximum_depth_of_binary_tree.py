from typing import List, Optional

import pytest

from explore.recursion.maximum_depth_of_binary_tree import Solution, TreeNode

s = Solution()


def build_tree_from_array(array: List[Optional[int]]) -> Optional[TreeNode]:
    l_array = len(array)
    if l_array == 0:
        return None
    elif l_array == 1:
        return TreeNode(val=array[0])

    def insert_node(root: Optional[TreeNode], i: int) -> Optional[TreeNode]:
        if i < l_array:
            temp = TreeNode(val=array[i])
            root = temp

            root.left = insert_node(root.left, 2*i+1)
            root.right = insert_node(root.right, 2*i+2)

        return root

    node = None
    node = insert_node(node, 0)
    return node


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [[3, 9, 20, None, None, 15, 7], 3],
        [[1, None, 2], 2],
        [[], 0],
        [[0], 1]
    ],
)
def test_problem(x: List[int], expected_result: int):
    tree_root = build_tree_from_array(x)
    assert s.maxDepth(tree_root) == expected_result
