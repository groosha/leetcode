from typing import List

import pytest

from explore.recursion.merge_two_sorted_lists import Solution
from tests.recursion.test_reverse_linked_list import make_linked_list, extract_values

s = Solution()


@pytest.mark.parametrize(
    "list1, list2, expected_result",
    [
        [[1, 2, 4], [1, 3, 4], [1, 1, 2, 3, 4, 4]],
        [[], [], []],
        [[], [0], [0]],
    ],
)
def test_problem(list1: List[int], list2: List[int], expected_result: List[int]):
    chain1 = make_linked_list(list1)
    chain2 = make_linked_list(list2)
    assert extract_values(s.mergeTwoLists(chain1, chain2)) == expected_result
