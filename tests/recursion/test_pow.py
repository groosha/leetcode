import pytest

from explore.recursion.pow import Solution

s = Solution()


@pytest.mark.parametrize(
    "x, n, expected_result",
    [
        [2.00000, 10, 1024.00000],
        [2.10000, 3, 9.26100],
        [2.00000, -2, 0.25000],
    ],
)
def test_problem(x: float, n: int, expected_result: float):
    assert round(s.myPow(x, n), 5) == expected_result
