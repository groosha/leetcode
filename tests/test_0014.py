from typing import List
import pytest

from problems.problem_0014_longest_common_prefix import Solution

s = Solution()


@pytest.mark.parametrize(
        "x, expected_result",
        [
            [["flower", "flow"], "flow"],
            [["flower", "flow", "flight"], "fl"],
            [["dog", "racecar", "car"], ""],
            [["ab", "a"], "a"],
            [["abab", "aba", ""], ""]
        ],
    )
def test_problem(x: List[str], expected_result: bool):
    assert s.longestCommonPrefix(x) == expected_result
