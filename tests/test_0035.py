from typing import List

import pytest

from problems.problem_0035_search_insert_position import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, target, expected_result",
    [
        [[1], 0, 0],
        [[1], 2, 1],
        [[1, 3, 5, 6], 5, 2],
        [[1, 3, 5, 6], 2, 1],
        [[1, 3, 5, 6], 7, 4],
    ],
)
def test_problem(nums: List[int], target: int, expected_result: int):
    assert s.searchInsert(nums, target) == expected_result
