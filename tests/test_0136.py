from typing import List

import pytest

from problems.problem_0136_single_number import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, expected_result",
    [
        [[2, 2, 1], 1],
        [[4, 1, 2, 1, 2], 4],
        [[1], 1]
    ],
)
def test_problem(nums: List[int], expected_result: int):
    assert s.singleNumber(nums) is expected_result
