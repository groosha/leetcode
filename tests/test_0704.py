from typing import List

import pytest

from problems.problem_0704_binary_search import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, target, expected_result",
    [
        [[-1, 0, 3, 5, 9, 12], 9, 4],
        [[-1, 0, 3, 5, 9, 12], 2, -1],
    ],
)
def test_problem(nums: List[int], target: int, expected_result: int):
    assert s.search(nums, target) == expected_result
