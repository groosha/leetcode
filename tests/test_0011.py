from typing import List
import pytest

from problems.problem_0011_container_with_most_water import Solution

s = Solution()


@pytest.mark.parametrize(
        "x, expected_result",
    [
        [[1, 1], 1],
        [[4, 3, 2, 1, 4], 16],
        [[1, 2, 1], 2],
        [[1, 8, 6, 2, 5, 4, 8, 3, 7], 49],
        [[2, 3, 4, 5, 18, 17, 6], 17],
        [[i for i in range(10001)] + [i for i in range(9999, 0, -1)], 50000000]
    ],
    )
def test_problem(x: List[int], expected_result: bool):
    assert s.maxArea(x) == expected_result
