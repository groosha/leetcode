import pytest

from problems.problem_0066_PlusOne import Solution


@pytest.mark.parametrize(
    "digits, expected_result",
    [
        [[1, 2, 3], [1, 2, 4]],
        [[4, 3, 2, 1], [4, 3, 2, 2]],
        [[9], [1, 0]],
        [[9, 8, 7, 6, 5, 4, 3, 2, 1, 0], [9, 8, 7, 6, 5, 4, 3, 2, 1, 1]],
    ],
)
def test_problem(digits: list[int], expected_result: list[int]):
    s = Solution()
    assert s.plusOne(digits) == expected_result
