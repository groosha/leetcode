from typing import List
import pytest

from problems.problem_0023_merge_k_sorted_lists import Solution
from tests.recursion.test_reverse_linked_list import make_linked_list, extract_values

s = Solution()


@pytest.mark.parametrize(
    "lists, expected_result",
    [
        [[[1, 4, 5], [1, 3, 4], [2, 6]], [1, 1, 2, 3, 4, 4, 5, 6]],
        [[], []],
        [[[]], []],
        [[[10], [9], [8], [7], [6], [5], [4], [3], [2], [1]], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]
    ],
)
def test_problem(lists: List[List[int]], expected_result: List):
    lists_to_test = [make_linked_list(x) for x in lists]
    result = s.mergeKLists(lists_to_test)
    assert extract_values(result) == expected_result
