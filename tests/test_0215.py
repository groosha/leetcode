from typing import List

import pytest

from problems.problem_0215_kth_largest_element_in_an_array import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, k, expected_result",
    [
        [[3, 2, 1, 5, 6, 4], 2, 5],
        [[3, 2, 3, 1, 2, 4, 5, 5, 6], 4, 4],
        [[-1, 2, 0], 1, 2],
        [[7, 6, 5, 4, 3, 2, 1], 5, 3],
    ],
)
def test_problem(nums: List[int], k: int, expected_result: int):
    assert s.findKthLargest(nums, k) == expected_result
