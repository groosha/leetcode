import pytest

from problems.problem_0067_AddBinary import Solution


@pytest.mark.parametrize(
    "a, b, expected_result",
    [
        ["11", "1", "100"],
        ["1010", "1011", "10101"]
    ],
)
def test_problem(a: str, b: str, expected_result: str):
    s = Solution()
    assert s.addBinary(a, b) == expected_result
