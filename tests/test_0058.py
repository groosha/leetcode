import pytest

from problems.problem_0058_length_of_last_word import Solution

s = Solution()


@pytest.mark.parametrize(
    "string, expected_result",
    [
        ["Hello World", 5],
        ["   fly me   to   the moon  ", 4],
        ["luffy is still joyboy", 6]
    ],
)
def test_problem(string: str, expected_result: int):
    assert s.lengthOfLastWord(string) == expected_result
