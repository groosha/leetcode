import pytest

from problems.problem_0013_roman_to_integer import Solution

s = Solution()


@pytest.mark.parametrize(
        "x, expected_result",
        [
            ["III", 3],
            ["IV", 4],
            ["IX", 9],
            ["XIV", 14],
            ["LVIII", 58],
            ["MCMXCIV", 1994]
        ],
    )
def test_problem(x: str, expected_result: bool):
    assert s.romanToInt(x) == expected_result
