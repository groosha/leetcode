from typing import List

import pytest

from problems.problem_0053_maximum_subarray import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, expected_result",
    [
        [[-2, 1, -3, 4, -1, 2, 1, -5, 4], 6],
        [[1], 1],
        [[5, 4, -1, 7, 8], 23],
        [[5, 4, 3, 2, 1, 0, -1, -2, -3], 15]
    ],
)
def test_problem(nums: List[int], expected_result: int):
    assert s.maxSubArray(nums) == expected_result
