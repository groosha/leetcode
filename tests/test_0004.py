from typing import List

import pytest

from problems.problem_0004_median_of_two_sorted_arrays import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums1, nums2, expected_result",
    [
        [[1, 3], [2], 2.0],
        [[1, 2], [3, 4], 2.5],
        [[1, 4, 10, 15], [1, 2, 5, 11, 12], 5.0]
    ],
)
def test_problem(nums1: List[int], nums2: List[int], expected_result: float):
    assert s.findMedianSortedArrays(nums1, nums2) == expected_result
