from typing import List

import pytest

from problems.problem_0001_TwoSum import Solution

s = Solution()


@pytest.mark.skip(reason="already tested")
@pytest.mark.parametrize(
        "nums, target, expected_result",
        [
            [[2, 7, 5, 11], 9, [0, 1]],
            [[-1, -2, -3, -4, -5], -8, [2, 4]],
            [[-3, 4, 3, 90], 0, [0, 2]],
        ],
    )
def test_problem(nums: List[int], target: int, expected_result: List[int]):
    assert s.twoSum(nums, target) == expected_result
