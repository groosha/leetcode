from typing import List

import pytest

from problems.problem_0002_add_two_numbers import Solution, ListNode

s = Solution()


def make_listnode(num: int, node: ListNode) -> ListNode:
    return ListNode(num, node)


@pytest.mark.skip(reason="already tested, needs uncommenting code")
@pytest.mark.parametrize(
        "l1, l2, expected_result",
    [
        [[2, 4, 3], [5, 6, 4], [7, 0, 8]],
        [[0], [0], [0]],
        [[9, 9, 9, 9, 9, 9, 9], [9, 9, 9, 9], [8, 9, 9, 9, 0, 0, 0, 1]],
    ],
    )
def test_problem(l1: List[int], l2: List[int], expected_result: List[int]):
    output = []
    for node in [l1, l2, expected_result]:
        out = ListNode(val=node[-1])
        if len(node) > 1:
            for num in reversed(node[:-1]):
                out = make_listnode(num, out)
        output.append(out)
        print("built")

    assert s.addTwoNumbers(output[0], output[1]) == output[2]
