import pytest

from problems.problem_0278_first_bad_version import Solution


@pytest.mark.parametrize(
    "versions_count, bad_version",
    [
        [5, 4],
        [10, 5]
    ],
)
def test_problem(versions_count: int, bad_version: int):
    s = Solution(bad_version)
    assert s.firstBadVersion(versions_count) == bad_version
