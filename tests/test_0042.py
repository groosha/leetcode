from typing import List

import pytest

from problems.problem_0042_trapping_rain_water import Solution

s = Solution()


@pytest.mark.parametrize(
    "heights, expected_result",
    [
        [[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1], 6],
        [[4, 2, 0, 3, 2, 5], 9],
    ],
)
def test_problem(heights: List[int], expected_result: int):
    assert s.trap(heights) == expected_result
