import pytest

from problems.problem_0028_find_the_index_of_the_first_occurence_in_a_string import Solution


@pytest.mark.parametrize(
    "haystack, needle, expected_result",
    [
        ["mississippi", "issip", 4],
        ["sadbutsad", "sad", 0],
        ["ssadbutsad", "sad", 1],
        ["abcde", "c", 2],
        ["leetcode", "leeto", -1],
    ],
)
def test_problem(haystack: str, needle: str, expected_result: int):
    s = Solution()
    assert s.strStr(haystack, needle) == expected_result
