import pytest

from problems.problem_0009_finding_palindromes import Solution

s = Solution()


@pytest.mark.skip(reason="already tested")
@pytest.mark.parametrize(
        "x, expected_result",
        [
            [121, True],
            [-121, False],
            [10, False],
            [-101, False],
            [11, True]
        ],
    )
def test_problem(x: int, expected_result: bool):
    assert s.isPalindrome(x) == expected_result
