from typing import List

import pytest

from problems.problem_0020_valid_parentheses import Solution

s = Solution()


@pytest.mark.parametrize(
    "string, expected_result",
    [
        ["()", True],
        ["()[]{}", True],
        ["(]", False],
        ["([)]", False],
    ],
)
def test_problem(string: str, expected_result: bool):
    assert s.isValid(string) is expected_result
