from typing import List

import pytest

from problems.problem_0017_letter_combinations_of_a_phone_number import Solution

s = Solution()


@pytest.mark.parametrize(
    "digits, expected_result",
    [
        ["23", ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"]],
        ["", []],
        ["2", ["a", "b", "c"]],
    ],
)
def test_problem(digits: str, expected_result: List[str]):
    assert sorted(s.letterCombinations(digits)) == sorted(expected_result)
