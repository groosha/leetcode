import pytest

from problems.problem_0008_atoi import Solution

solution = Solution()


@pytest.mark.skip(reason="already tested")
@pytest.mark.parametrize(
        "s, expected_result",
        [
            ["42", 42],
            ["   -42", -42],
            ["4193 with words", 4193],
            ["words and 987", 0],
            ["-91283472332", -2147483648],
            ["+1", 1],
            [" ", 0]
        ],
    )
def test_problem(s: str, expected_result: int):
    assert solution.myAtoi(s) == expected_result
