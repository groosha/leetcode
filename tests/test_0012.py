import pytest

from problems.problem_0012_integer_to_roman import Solution

s = Solution()


@pytest.mark.parametrize(
        "x, expected_result",
        [
            [3, "III"],
            [4, "IV"],
            [9, "IX"],
            [14, "XIV"],
            [58, "LVIII"],
            [1994, "MCMXCIV"]
        ],
    )
def test_problem(x: int, expected_result: bool):
    assert s.intToRoman(x) == expected_result
