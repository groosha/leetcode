import pytest

from problems.problem_0006_zigzag_conversion import Solution

solution = Solution()


@pytest.mark.skip(reason="already tested")
@pytest.mark.parametrize(
        "s, numRows, expected_result",
        [
            ["PAYPALISHIRING", 3, "PAHNAPLSIIGYIR"],
            ["PAYPALISHIRING", 4, "PINALSIGYAHRPI"],
            ["A", 1, "A"],
            ["AB", 1, "AB"]
        ],
    )
def test_problem(s: str, numRows: int, expected_result: str):
    assert solution.convert(s, numRows) == expected_result
