from typing import List

import pytest

from problems.problem_0071_simplify_path import Solution

s = Solution()


@pytest.mark.parametrize(
    "path, expected_result",
    [
        ["/home/", "/home"],
        ["/../", "/"],
        ["/home//foo/", "/home/foo"],
    ],
)
def test_problem(path: str, expected_result: str):
    assert s.simplifyPath(path) == expected_result
