from typing import List

import pytest

from problems.problem_0739_daily_temperatures import Solution

s = Solution()


@pytest.mark.parametrize(
    "temperatures, expected_result",
    [
        [[73, 74, 75, 71, 69, 72, 76, 73], [1, 1, 4, 2, 1, 1, 0, 0]],
        [[30, 40, 50, 60], [1, 1, 1, 0]],
        [[30, 60, 90], [1, 1, 0]],
    ],
)
def test_problem(temperatures: List[int], expected_result: List[int]):
    assert s.dailyTemperatures(temperatures) == expected_result
