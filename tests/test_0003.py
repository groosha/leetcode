import pytest

from problems.problem_0003_longest_substring_without_repeats import Solution

s = Solution()


@pytest.mark.skip(reason="already tested")
@pytest.mark.parametrize(
        "x, expected_result",
        [
            ["abcabcbb", 3],
            ["bbbbb", 1],
            ["pwwkew", 3],
            ["dvdf", 3]
        ],
    )
def test_problem(x: str, expected_result: int):
    assert s.lengthOfLongestSubstring(x) == expected_result
