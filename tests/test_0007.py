from typing import List

import pytest

from problems.problem_0007_reverse_integer import Solution

s = Solution()


@pytest.mark.skip(reason="already tested")
@pytest.mark.parametrize(
        "x, expected_result",
        [
            [123, 321],
            [-123, -321],
            [120, 21],
            [0, 0],
            [1534236469, 0]
        ],
    )
def test_problem(x: int, expected_result: int):
    assert s.reverse(x) == expected_result

