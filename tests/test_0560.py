from typing import List

import pytest

from problems.problem_0560_subarray_sum_equals_k import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, k, expected_result",
    [
        [[1, 1, 1], 2, 2],
        [[1, 2, 3], 3, 2],
        [[1], 1, 1],
        [[1], 2, 0],
        [[1, -1, 0], 0, 3]
    ],
)
def test_problem(nums: List[int], k: int, expected_result: int):
    assert s.subarraySum(nums, k) == expected_result
