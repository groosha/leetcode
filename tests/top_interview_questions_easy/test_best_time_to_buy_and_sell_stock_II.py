from typing import List

import pytest

from explore.top_interview_questions_easy.best_time_to_buy_and_sell_stock_II import Solution


s = Solution()


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [[7, 1, 5, 3, 6, 4], 7],
        [[1, 2, 3, 4, 5], 4],
        [[7, 6, 4, 3, 1], 0]
    ],
)
def test_problem(x: List[int], expected_result: int):
    assert s.maxProfit(x) == expected_result
