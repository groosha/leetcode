from typing import List

import pytest

from explore.top_interview_questions_easy.single_number import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, expected_result",
    [
        [[2, 2, 1], 1],
        [[4, 1, 2, 1, 2], 4],
        [[1], 1],
    ],
)
def test_problem(nums: List[int], expected_result: int):
    assert s.singleNumber(nums) == expected_result
