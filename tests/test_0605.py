from typing import List

import pytest

from problems.problem_0605_can_place_flowers import Solution

s = Solution()


@pytest.mark.parametrize(
    "flowerbed, n, expected_result",
    [
        [[0], 1, True],
        [[1], 1, False],
        [[1, 0, 0, 0, 1], 1, True],
        [[1, 0, 0, 0, 1], 2, False],
        [[1, 0, 0, 0, 0, 1], 2, False],
        [[1, 0, 0, 0, 1, 0, 0], 2, True],
        [[1, 0], 1, False],
        [[0, 0], 2, False],
        [[0, 0, 0, 0, 1], 2, True]
    ],
)
def test_problem(flowerbed: List[int], n: int, expected_result: bool):
    assert s.canPlaceFlowers(flowerbed, n) is expected_result
