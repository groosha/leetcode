from typing import List
import pytest

from problems.problem_0026_remove_duplicates_from_sorted_array import Solution

s = Solution()


@pytest.mark.parametrize(
    "x, expected_result",
    [
        [[1, 1, 2], (2, [1, 2, 2])],
        [[0, 0, 1, 1, 1, 2, 2, 3, 3, 4], (5, [0, 1, 2, 3, 4, 2, 2, 3, 3, 4])],
    ],
)
def test_problem(x: List[int], expected_result: bool):
    assert s.removeDuplicates(x) == expected_result
