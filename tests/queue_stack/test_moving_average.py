from typing import List

import pytest

from explore.queue_stack.moving_average import MovingAverage


@pytest.mark.parametrize(
    "size, values, expected",
    [
        [1, [2, 2, 2, 2, 2, 2], [2, 2, 2, 2, 2, 2]],
        [2, [2, 4, 4, 8], [2, 3, 4, 6]]
    ]
)
def test_moving_average(size: int, values: List[int], expected: List[int]):
    x = MovingAverage(size)
    assert len(values) == len(expected)
    for i in range(len(values)):
        result = x.next(values[i])
        assert result == expected[i]
