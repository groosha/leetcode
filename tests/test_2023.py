from typing import List

import pytest

from problems.problem_2023_number_of_pairs_of_strings_with_concatenation_equal_to_target import Solution

s = Solution()


@pytest.mark.parametrize(
    "nums, target, expected_result",
    [
        [["777", "7", "77", "77"], "7777", 4],
        [["123", "4", "12", "34"], "1234", 2],
        [["1", "1", "1"], "11", 6],
        [["74", "1", "67", "1", "74"], "174", 4]
    ],
)
def test_problem(nums: List[str], target: str, expected_result: int):
    assert s.numOfPairs(nums, target) == expected_result
