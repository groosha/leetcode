class Node:
    def __init__(self, val: int):
        self.val = val
        self.next = None


class MyLinkedList:
    def __init__(self):
        self.size = 0
        # Fake node at the beginning
        self.head = Node(0)

    def get(self, index: int) -> int:
        if index < 0 or index >= self.size:
            return -1
        cur = self.head
        for i in range(index + 1):
            cur = cur.next
        return cur.val

    def addAtHead(self, val: int) -> None:
        self.addAtIndex(0, val)

    def addAtTail(self, val: int) -> None:
        self.addAtIndex(self.size, val)

    def addAtIndex(self, index: int, val: int) -> None:
        if index > self.size:
            return

        self.size += 1

        # Start at fake node
        cur = self.head

        for i in range(index):
            cur = cur.next
        new_node = Node(val)
        new_node.next = cur.next
        cur.next = new_node

    def deleteAtIndex(self, index: int) -> None:
        if index < 0 or index >= self.size:
            return

        self.size -= 1

        cur = self.head
        for i in range(index):
            cur = cur.next
        cur.next = cur.next.next

# Your MyLinkedList object will be instantiated and called as such:
# obj = MyLinkedList()
# param_1 = obj.get(index)
# obj.addAtHead(val)
# obj.addAtTail(val)
# obj.addAtIndex(index,val)
# obj.deleteAtIndex(index)


if __name__ == '__main__':
    obj = MyLinkedList()
    param_1 = obj.get(4)
    assert param_1 == -1
    obj.addAtHead(1)
    assert obj.get(0) == 1
    obj.addAtTail(3)
    assert obj.get(1) == 3
    obj.addAtIndex(1, 2)
    assert obj.get(1) == 2
    obj.deleteAtIndex(1)
    assert obj.get(1) == 3

    obj = MyLinkedList()
    obj.addAtHead(1)
    obj.addAtTail(3)
    obj.addAtIndex(1, 2)
    assert obj.get(1) == 2
    obj.deleteAtIndex(0)
    assert obj.get(0) == 2

    obj = MyLinkedList()
    obj.addAtIndex(0, 10)
    obj.addAtIndex(0, 20)
    obj.addAtIndex(1, 30)
    assert obj.get(0) == 20

    obj = MyLinkedList()
    obj.addAtHead(7)
    obj.addAtHead(2)
    obj.addAtHead(1)
    obj.addAtIndex(3, 0)
    obj.deleteAtIndex(2)
    obj.addAtHead(6)
    obj.addAtTail(4)
    assert obj.get(4) == 4
    obj.addAtHead(4)
    obj.addAtIndex(5, 0)
    obj.addAtHead(6)
