"""
You are given an integer array prices where prices[i] is the price of a given stock on the ith day.

On each day, you may decide to buy and/or sell the stock.
You can only hold at most one share of the stock at any time.
However, you can buy it then immediately sell it on the same day.

Find and return the maximum profit you can achieve.
"""
from typing import List


class Solution:
    def maxProfit(self, prices: List[int]) -> int:
        profit = 0
        peak = prices[0]
        valley = prices[0]

        i = 0
        max_index = len(prices) - 1
        while i < max_index:
            while i < max_index and prices[i] >= prices[i+1]:
                i += 1
            valley = prices[i]

            while i < max_index and prices[i] <= prices[i+1]:
                i += 1
            peak = prices[i]

            profit += peak - valley
        return profit