class Solution:
    """
    if n is odd:
    x^n = x * (x^2)^((n-1)/2)

    if n is even:
    x^n = (x^2)^(n/2)
    """

    def myPow(self, x: float, n: int) -> float:

        def function(base: float = x, exponent: int = abs(n)):
            if exponent == 0:
                return 1
            elif exponent % 2 == 0:
                return function(base * base, exponent // 2)
            else:
                return base * function(base * base, (exponent - 1) // 2)

        f = function()

        return float(f) if n >= 0 else 1 / f
