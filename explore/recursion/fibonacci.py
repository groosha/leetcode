class Solution:
    def fib(self, n: int) -> int:
        cache = {}

        def _fib(x):
            if x in cache:
                return cache[x]
            if x < 2:
                result = x
            else:
                result = _fib(x - 1) + _fib(x - 2)

            cache[x] = result
            return result

        if n < 2:
            return n
        else:
            return _fib(n)
