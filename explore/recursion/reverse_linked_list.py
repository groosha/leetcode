from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def change_node_role(self, node: Optional[ListNode]) -> Optional[ListNode]:
        if node is None:
            return None
        # If there are no more nodes, then it's the new head
        if node.next is None:
            return node

        next_head = self.change_node_role(node.next)
        node.next = None

        current = next_head
        while current.next is not None:
            current = current.next

        current.next = node
        return next_head

    def reverseList(self, head: Optional[ListNode]) -> Optional[ListNode]:
        new_head = self.change_node_role(head)
        return new_head


