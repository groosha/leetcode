def fibonacci_slow(n):
    """
    :type n: int
    :rtype: int
    """
    if n < 2:
        return n
    else:
        return fibonacci_slow(n - 1) + fibonacci_slow(n - 2)


def fibonacci_better(n):
    cache = {}

    def _fib(x):
        if x in cache:
            return cache[x]
        if x < 2:
            result = x
        else:
            result = _fib(x-1) + _fib(x-2)

        cache[x] = result
        return result

    if n < 2:
        return n
    else:
        return _fib(n)


if __name__ == '__main__':
    val = 10
    for i in range(100_000):
        fibonacci_slow(val)
    for i in range(100_000):
        fibonacci_better(val)
