from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def explore_node(self, root: TreeNode, current_depth: int) -> int:
        value_left = current_depth
        value_right = current_depth
        if root.left is not None:
            value_left = self.explore_node(root.left, current_depth + 1)
        if root.right is not None:
            value_right = self.explore_node(root.right, current_depth + 1)
        return max(value_left, value_right)

    def maxDepth(self, root: Optional[TreeNode]) -> int:
        if root is None:
            return 0
        return self.explore_node(root, 1)
