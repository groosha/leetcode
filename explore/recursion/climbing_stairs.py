class Solution:
    def climbStairs(self, n: int) -> int:
        cache = {}

        def count_climb_for(x):
            if x in cache:
                return cache[x]
            if x <= 3:
                result = x
            else:
                result = count_climb_for(x - 1) + count_climb_for(x - 2)

            cache[x] = result
            return result

        return count_climb_for(n)
