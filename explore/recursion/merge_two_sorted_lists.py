from typing import Optional, List


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def merge(
            self,
            list1: Optional[ListNode],
            list2: Optional[ListNode],
            result: List[int]
    ) -> List[int]:
        # If at least one list is empty, then we can chain the rest of another list
        if list1 is None or list2 is None:
            if list1 is None and list2 is None:
                return []
            non_empty = list1 or list2
            while non_empty is not None:
                result.append(non_empty.val)
                non_empty = non_empty.next
            return result

        if list1.val <= list2.val:
            result.append(list1.val)
            return self.merge(list1.next, list2, result)
        else:
            result.append(list2.val)
            return self.merge(list1, list2.next, result)

    def mergeTwoLists(
            self,
            list1: Optional[ListNode],
            list2: Optional[ListNode]
    ) -> Optional[ListNode]:
        if list1 is None and list2 is None:
            return None

        result_array = self.merge(list1, list2, [])
        result = ListNode(val=result_array[0])
        current = result
        for i in range(1, len(result_array)):
            next_node = ListNode(val=result_array[i])
            current.next = next_node
            current = next_node
        return result
