from typing import List


class Solution:
    def build_row(self, num: int):
        if num == 0:
            return [1]
        previous_row = self.build_row(num-1)
        array = [1]
        for i in range(1, num):
            array.append(previous_row[i-1] + previous_row[i])
        array.append(1)
        return array

    def getRow(self, rowIndex: int) -> List[int]:
        return self.build_row(rowIndex)
