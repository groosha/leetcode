from typing import Optional


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def make_search(self, subtree: Optional[TreeNode], target_value: int):
        if subtree is None:
            return None

        if subtree.val == target_value:
            return subtree

        if subtree.left is not None:
            left_part = self.make_search(subtree.left, target_value)
            if left_part is not None and left_part.val == target_value:
                return left_part

        if subtree.right is not None:
            right_part = self.make_search(subtree.right, target_value)
            if right_part is not None and right_part.val == target_value:
                return right_part

    def searchBST(self, root: Optional[TreeNode], val: int) -> Optional[TreeNode]:
        result = self.make_search(subtree=root, target_value=val)
        return result
