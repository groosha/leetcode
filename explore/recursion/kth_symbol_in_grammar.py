class Solution:
    def kthGrammar(self, n: int, k: int) -> int:
        if n == 1:
            return 0

        middle = 2 ** (n - 2)

        # If k lies in the first half of N-th string, then it's simply in (N-1)th string
        if k <= middle:
            return self.kthGrammar(n-1, k)

        prev_row_res = self.kthGrammar(n-1, k-middle)
        return int(not prev_row_res)
