# Definition for singly-linked list.
from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def make_swap(self, head: ListNode):
        if head is None:
            return None
        if head.next is None:
            return head
        next_swapped = self.make_swap(head.next.next)
        new_head = head.next
        new_head.next = head
        new_head.next.next = next_swapped
        return new_head

    def swapPairs(self, head: Optional[ListNode]) -> Optional[ListNode]:
        new_head = self.make_swap(head)
        return new_head
